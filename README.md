# Deep Learning Examples
This is a repository of some examples in Deep Learning implemented in PyTorch/TensorFlow.

Contact info: yuxuan.chen@fu-berlin.de

# Table of Content

## [1. Basic](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/tree/master/01-Basic)
#### 1.1 Image Classification with Logistic Regression
Logistic regression on a subset of MNIST dataset containing 20000 samples. Implemented in: 
- [PyTorch](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_regression_pytorch.ipynb): 
91.1% validation accuracy
- [TensorFlow](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/01-Basic/imaga_classfication_with_regression_tf.ipynb):
90.8% validation accuracy

#### 1.2 Image Classification with Neural Network
A simple neural network of one hidden layer on the same dataset. Implemented in:
- [PyTorch](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_neural_network_pytorch.ipynb):
96.9% validation accuracy
- [TensorFlow](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_neural_network_tf.ipynb)
96.4% validation accuracy
 
#### 1.3 Image Classification with CNN
CNN of 2 convolutional layers on the same dataset. Implemented in: 
- [PyTorch](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_CNN_pytorch.ipynb):
98.9% validation accuracy
- [TensorFlow](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_CNN_tf.ipynb):
97.6% validation accuracy

## [2. Intermediate](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/tree/master/2.%20Intermediate)
#### 2.1 Sequence Classification with RNN
Use RNN and its variations to classify DNA sequences. Implemented in: 
- [PyTorch](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/02-Intermediate/sequence_classification_with_RNN_pytorch.ipynb): 
100% validation accuracy
- [TensorFlow](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/02-Intermediate/sequence_classification_with_RNN_tf.ipynb):
100% validation accuracy
#### 2.2 VAE
We use VAE and CVAE of 2-dim latent space to generate pictures learnt from 20000-sized MNIST dataset. Implemented in:
- [PyTorch](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/02-Intermediate/vae_pytorch.ipynb)
- [TensorFlow](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/02-Intermediate/vae_tf.ipynb)
#### 2.3 GAN
We use vanilla GAN of 32-dim latent space to generate from 60000-sized MNIST dataset. Implemented in:
- [PyTorch](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/blob/master/02-Intermediate/gan_pytorch.ipynb)

## [3. Advanced](https://git.imp.fu-berlin.de/yuxuac94/deep-learning-examples/-/tree/master/3.%20Advanced)


## Dependencies
- **Python 3.5+**
- **PyTorch 1.5.0**
- **TensorFlow 2.2.0**
